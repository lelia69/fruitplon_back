import Joi from "joi";

export const userSchema = Joi.object({
    email: Joi.string().email().required().normalize(),

    password: Joi.string().pattern(new RegExp("^[a-zA-Z0-9!@#^$%&*]{3,25}$")).required(),
});
