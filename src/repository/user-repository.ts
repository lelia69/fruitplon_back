import { ResultSetHeader, RowDataPacket } from "mysql2";
import { getConnection } from "../database";
import { User } from "../entity/User";

interface IUser extends RowDataPacket {
    id: number;
    password: string;
    email: string;
}
export class UserRepository {
    /**
     * Method that persists a user
     * @param {User} user user to persist
     */
    static async add(user: User) {
        const connection = await getConnection();
        const [rows] = await connection.query(
            "INSERT INTO user (email,password) VALUES (?,?)",
            [user.email, user.password]
        );
        user.id = (rows as ResultSetHeader).insertId;
        connection.destroy();
        return user;
    }

    /**
     * Fetch user by email
     * @param {string} email email of user
     */
    static async findByEmail(email: string) {
        const connection = await getConnection();
        const [rows] = await connection.query<IUser[]>(
            "SELECT * FROM user WHERE email=?",
            [email]
        );
        connection.destroy();
        
        if (rows.length === 1) {
            const user = new User();
            Object.assign(user, {
                email: rows[0]?.email,
                password: rows[0]?.password,
                id: rows[0]?.id,
            });
            return user;
        }
        return null;
    }
}
