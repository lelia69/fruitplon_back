import express from "express";
import cors from "cors";
import { userController } from "./controller/user-controller";
import passport from "passport";
import { configurePassport } from "./utils/token";

//On appel la fonction qui configure la stratégie JWT
configurePassport();

export const server = express();

//On dit au server express d'utiliser passport pour autoriser ou non l'accès aux routes
server.use(passport.initialize());

server.use(express.json());
server.use(cors({ origin: ["localhost"] }));

server.use("/api/user", userController);
