import { createConnection } from "mysql2/promise";

export const getConnection = async () => {
    const AppDataSource = await createConnection({
        uri: process.env.DATABASE_URL,
    });
    return AppDataSource;
};
