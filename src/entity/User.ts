export class User {
    id!: number;
    email!: string;
    password?: string;

    //Permet de contrôler comment l'objet sera transformer en JSON
    toJSON() {
        return {
            id: this.id,
            email: this.email,
        };
    }
}
