import { Router } from "express";
import bcrypt from "bcrypt";
import passport from "passport";
import { User } from "../entity/User";
import { generateToken } from "../utils/token";
import { UserRepository } from "../repository/user-repository";
import { userSchema } from "../validator/user-validator";
import { ValidationResult } from "joi";

export const userController = Router();

userController.post("/", async (req, res) => {
    try {
        const newUser = new User();

        const { error, value }: ValidationResult<User> = userSchema.validate(
            req.body
        );

        if (error) {
            return res.status(400).json({ error: error.message });
        }

        Object.assign(newUser, value);
        const exists = await UserRepository.findByEmail(newUser.email);
        if (exists) {
            res.status(400).json({ error: "Email already taken" });
            return;
        }
        //On hash le mdp du user pour pas le stocker en clair
        if (newUser.password) {
            newUser.password = await bcrypt.hash(newUser.password, 11);
        }

        await UserRepository.add(newUser);
        return res.status(201).json(newUser);
    } catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }
});

userController.post("/login", async (req, res) => {
    try {
        const user = await UserRepository.findByEmail(req.body.email);
        if (user && user.password) {
            const samePassword = await bcrypt.compare(
                req.body.password,
                user.password
            );
            if (samePassword) {
                res.json({
                    user,
                    token: generateToken({
                        email: user.email,
                        id: user.id,
                    }),
                });
                return;
            }
        }
        res.status(401).json({ error: "Wrong email and/or password" });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

/**
 * On indique à passport que cette route est protégée par un JWT, si on tente d'y
 * accéder sans JWT dans le header ou avec un jwt invalide, l'accès sera refusé
 * et express n'exécutera pas le contenu de la route
 */
userController.get(
    "/account",
    passport.authenticate("jwt", { session: false }),
    (req, res) => {
        //comme on est dans une route protégée, on peut accéder à l'instance de User correspondant
        //au token avec req.user
        res.json(req.user);
    }
);
