import jwt from "jsonwebtoken";
import passport from "passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import "dotenv/config";
import { User } from "../entity/User";
import { UserRepository } from "../repository/user-repository";

export function generateToken(payload: Partial<User>) {
    const token = jwt.sign(payload, process.env.TOKEN_SECRET!, {
        expiresIn: 60 * 60,
        algorithm: "HS512",
    });
    return token;
}

export function generateRefreshToken(user: Partial<User>) {
    return jwt.sign(user, process.env.REFRESH_TOKEN_SECRET!, {
        expiresIn: "1y",
        algorithm: "HS512",
    });
}

export function configurePassport() {
    passport.use(
        new Strategy(
            {
                jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
                secretOrKey: process.env.TOKEN_SECRET,
            },
            async (payload, done) => {
                try {
                    const user = await UserRepository.findByEmail(
                        payload.email
                    );

                    if (user) {
                        return done(null, {
                            email: user.email,
                            id: user.id,
                        });
                    }

                    return done(null, false);
                } catch (error) {
                    console.log(error);
                    return done(error, false);
                }
            }
        )
    );
}
